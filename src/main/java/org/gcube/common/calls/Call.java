package org.gcube.common.calls;

import java.util.HashMap;
import java.util.Map;

public class Call {

	private Map<String, Object> properties = new HashMap<String, Object>();
	
	public Map<String, Object> properties() {
		return properties;
	}
}
