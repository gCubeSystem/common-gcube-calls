package org.gcube.common.calls.interceptors;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.gcube.common.calls.Call;
import org.gcube.common.calls.Interceptor;
import org.gcube.common.calls.Request;
import org.gcube.common.calls.Response;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthorizationInterceptor implements Interceptor {

	private Logger logger = LoggerFactory.getLogger(AuthorizationInterceptor.class);

	@Override
	public void handleRequest(Request request, Call call) {
		Secret secret = SecretManagerProvider.get();

		if (secret!=null) {
			try { 
				Map<String, String> header = secret.getHTTPAuthorizationHeaders();
				Objects.requireNonNull(header);
				for (Entry<String, String> entry : header.entrySet()) {
						request.addHeader(entry.getKey(), entry.getValue());	
						logger.trace("setting {} : {}", entry.getKey(), entry.getValue());
				}
			}catch (Exception e) {
				logger.error("error setting header for secret",e);
			}
		} else logger.trace("secret is not set");
	}

	@Override
	public void handleResponse(Response context, Call callContext) {}
}
